import React, { Component } from 'react';
import {
   BrowserRouter as Router,
   Link,
   Route,
   Switch
} from 'react-router-dom';
import './css/App.css';

// PAGES
import Login from "./telas/login/Login";

class App extends Component {
  render() {
    return (
      <React.Fragment>
          <Router>
            <Switch>
              <Route exact path="/" component={Login} />
            </Switch>
          </Router>
      </React.Fragment>
    );
  }
}

export default App;
